package com.students.tracking.service;

import java.util.List;

import com.students.tracking.model.User;


public interface UserService {
	
	User findById(int id);
	
	User findByLogin(String login);
	
	void saveUser(User user);
	
	void updateUser(User user);
	
	void deleteUserByLogin(String login);

	List<User> findAllUsers(); 
	
	boolean isUserLoginUnique(Integer id, String login);

}