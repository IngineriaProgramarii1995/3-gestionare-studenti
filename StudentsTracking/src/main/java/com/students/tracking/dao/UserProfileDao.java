package com.students.tracking.dao;

import java.util.List;

import com.students.tracking.model.UserProfile;


public interface UserProfileDao {

	List<UserProfile> findAll();
	
	UserProfile findByType(String type);
	
	UserProfile findById(int id);
}
