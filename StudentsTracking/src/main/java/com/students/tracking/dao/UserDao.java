package com.students.tracking.dao;

import java.util.List;

import com.students.tracking.model.User;


public interface UserDao {

	User findById(int id);
	
	User findByLogin(String login);
	
	void save(User user);
	
	void deleteByLogin(String login);
	
	List<User> findAllUsers();

}

